# gruntjs.com

module.exports = (grunt) ->
	'use strict'

	require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks)

	debug = !!grunt.option('debug')

	grunt.initConfig
		watch:
			stylus:
				files: 'styles/**'
				tasks: 'stylus'
		stylus:
			compile:
				options:
					'include css': true
					urlfunc: 'embedurl'
					define:
						DEBUG: debug
					paths: [
						'tamia'
					]
				files:
					'todo/build/styles.css': 'styles/index.styl'

	grunt.registerTask 'default', ['stylus']
	grunt.registerTask 'deploy', ['stylus']
