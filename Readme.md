# TopTal Todo

© [Artem Sapegin](http://sapegin.me)

Deployed app: [http://tttd.meteor.com/](http://tttd.meteor.com/).


## Used tools

* [Meteor](http://www.meteor.com/)
* [Tâmia](https://github.com/sapegin/tamia) (my front-end framework)
* [Stylus](http://learnboost.github.io/stylus/)


## Project structure

* Server code: `todo/server/server.js`.
* Client code: `todo/client/client.js`.
* Styles: `styles/`.


## Install & run

First install Meteor:

```
curl https://install.meteor.com | /bin/sh
```

Then `cd` to `todo` folder and run the project:

```
meteor
```

Now open [http://localhost:3000/](http://localhost:3000/) in your browser.


## REST API

### Login

```
curl --data "password=test&user=artem@sapegin.ru" http://localhost:3000/api/login/
```

### List of todos

```
curl -H "X-Login-Token: umyndTx9XYQJ3yGZL" -H "X-User-Id: 8Mvsq8MZwR5zmt5pw" http://localhost:3000/api/todos/
```

### Single todo

```
curl  -H "X-Login-Token: umyndTx9XYQJ3yGZL" -H "X-User-Id: 8Mvsq8MZwR5zmt5pw" http://localhost:3000/api/todos/4sYr5j6bCoRfJM5GW
```


### Add a todo

```
curl  -H "X-Login-Token: umyndTx9XYQJ3yGZL" -H "X-User-Id: 8Mvsq8MZwR5zmt5pw" --data "title=NewToDo" -X POST http://localhost:3000/api/todos/
```

### Remove a todo

```
curl  -H "X-Login-Token: umyndTx9XYQJ3yGZL" -H "X-User-Id: 8Mvsq8MZwR5zmt5pw" -X DELETE http://localhost:3000/api/todos/MASC3EGCctwxkPLxT
```

### Update a todo

```
curl  -H "X-Login-Token: umyndTx9XYQJ3yGZL" -H "X-User-Id: 8Mvsq8MZwR5zmt5pw" --data "title=NewTitle" -X PUT http://localhost:3000/api/todos/DNt7cNFqf2Tw5h5z8
```
