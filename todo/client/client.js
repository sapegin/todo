/* Author: Artem Sapegin, http://sapegin.me, 2013 */

Todos = new Meteor.Collection('todos');

Meteor.subscribe('todos');

Session.set('edit_id', null);


/**
 * Main template
 */

Template.main.events({
	'click .js-logout': function(event, container) {
		event.preventDefault();
		Meteor.logout();
	}
});


/**
 * Todos list template
 */

Template.todos.todos = function() {
    return Todos.find({}, {sort: {order: 1}});
};

Template.todos.events({
	'submit .js-add-form': function(event, container) {
		event.preventDefault();
		var titleElem = container.find('.js-add-title');
		var title = $.trim(titleElem.value);
		if (title) {
			Meteor.call('add', {title: title});
		}
		titleElem.value = '';
	}
});


/**
 * Todo template
 */

Template.todo.editing = function() {
	return Session.equals('edit_id', this._id);
};

Template.todo.events({
	'change .js-toggle': function() {
		Meteor.call('update', this._id, {done: !this.done});
	},
	'click .js-remove': function() {
		event.preventDefault();
		Meteor.call('remove', this._id);
	},
	'dblclick .js-title': function() {
		Session.set('edit_id', this._id);
	},
	'keydown .js-title-field, keyup .js-title-field, focusout .js-title-field': function(event) {
		var done = function() {
			event.preventDefault();
			Session.set('edit_id', null);
		};

		if (event.type === 'keydown' && event.which === 27) {
			// Esc: cancel editing
			done();
			return;
		}
		if (event.type === 'keyup' && event.which === 13 || event.type === 'focusout') {
			var value = $.trim(event.target.value);
			if (value) {
				Meteor.call('update', this._id, {title: value});
			}
			else {
				Meteor.call('remove', this._id);
			}
			done();
		}
	},
	'dragstart .js-drag-target': function(event) {
		event.dataTransfer.effectAllowed = 'move';
		event.dataTransfer.setData('text', this._id);
	},
	'dragover .js-drag-target': function(event, container) {
		event.preventDefault();
		event.dataTransfer.dropEffect = 'move';
		$(container.firstNode).addClass('is-drag-over');
	},
	'dragleave .js-drag-target': function(event, container) {
		$(container.firstNode).removeClass('is-drag-over');
	},
	'dragend .js-drag-target': function(event, container) {
		$(container.firstNode).siblings('.is-drag-over').removeClass('is-drag-over');
	},
	'drop': function(event) {
		event.preventDefault();
		var id = event.dataTransfer.getData('text');
		if (id === this._id) return;
		Meteor.call('insertAfter', id, this._id);
	}
});


/**
 * Login / register form
 */

Template.login.events({
	'submit .js-login-form': function(event, container) {
		function error(message) {
			var errorElem = $(container.find('.js-error'));
			if (message) {
				errorElem.html(message);
				errorElem.removeClass('is-hidden');
				container.find('.js-login').focus();
				return false;
			}
			else {
				errorElem.addClass('is-hidden');
				return true;
			}
		}

		event.preventDefault();
		error(false);
	
		var email = $.trim(container.find('.js-login').value);
		var password = $.trim(container.find('.js-password').value);

		if (!email || !password) return error('All fields are required.');

		var isLogin = $(container.firstNode).hasClass('show-login');

		if (isLogin) {
			Meteor.loginWithPassword(email, password, function(err) {
				if (err) {
					error('Email or password is incorrect.');
				}
			});
		}
		else {
			Accounts.createUser({ email: email, password: password }, function(err) {
				if (err) {
					error('Registration error.');
				}
			});
		}

		return false;
	},
	'click .js-toggle': function(event, container) {
		event.preventDefault();
		container = $(container.firstNode);
		var isLogin = !container.hasClass('show-login');
		container.toggleClass('show-login', isLogin);
		container.toggleClass('show-register', !isLogin);
	}
});
