/* Author: Artem Sapegin, http://sapegin.me, 2013 */

// Database
Todos = new Meteor.Collection('todos');


// Publish logged in user’s todo to client
Meteor.publish('todos', function () {
	return Todos.find({userId: this.userId});
});


/*
 * Todo manager
 *
 * Could be used from client (via server methods) or from REST API
 */
var TodoManager = {
	/**
	 * Returns all todos for logged in user
	 * 
	 * @param {String} userId
	 * @return {Array}
	 */
	list: function(userId) {
		return Todos.find({userId: userId}, {sort: {order: 1}});
	},

	/**
	 * Returns single todo
	 * 
	 * @param {String} userId
	 * @param {String} id Todo ID
	 * @return {Object}
	 */
	get: function(userId, id) {
		return Todos.findOne({userId: userId, _id: id});
	},

	/**
	 * Add todo
	 * 
	 * @param {String} userId
	 * @param {Object} data Todo data:
	 *                 data.title Todo title
	 * @return {String} Added todo ID
	 */
	add: function(userId, data) {
		if (!data.title) return false;
		var last = Todos.findOne({userId: userId}, {sort: {order: -1}});
		var lastOrder = last && last.order || 1;
		return Todos.insert({
			title: data.title,
			done: false,
			order: lastOrder + 1,
			userId: userId
		});
	},

	/**
	 * Remove todo
	 * 
	 * @param {String} userId
	 * @param {String} id Todo ID
	 */
	remove: function(userId, id) {
		Todos.remove({_id: id, userId: userId});
	},

	/**
	 * Modify todo
	 * 
	 * @param {String} userId
	 * @param {String} id Todo ID
	 * @param {Object} data Todo data:
	 *                 data.title Todo title
	 */
	update: function(userId, id, data) {
		return Todos.update({_id: id, userId: userId}, {$set: data});
	},

	/**
	 * Move todo after another todo
	 * 
	 * @param {String} userId
	 * @param {String} id Todo ID
	 * @param {String} targetId ID of todo after which first todo should be placed
	 */
	insertAfter: function(userId, id, targetId) {
		var order = Todos.findOne({userId: userId, _id: id}).order;
		var targetOrder = Todos.findOne({userId: userId, _id: targetId}).order;

		if (order > targetOrder) {
			Todos.update({userId: userId, order: {$gt: targetOrder, $lt: order}}, {$inc: {order: 1}}, {multi: true});
			Todos.update({userId: userId, _id: id}, {$set: {order: targetOrder + 1}});
		}
		else {
			Todos.update({userId: userId, order: {$gt: order, $lte: targetOrder}}, {$inc: {order: -1}}, {multi: true});
			Todos.update({userId: userId, _id: id}, {$set: {order: targetOrder}});
		}
	}
};


// Methods to call front client
Meteor.methods({
	add: function(data) {
		return TodoManager.add(this.userId, data);
	},

	remove: function(id) {
		return TodoManager.remove(this.userId, id);
	},

	update: function(id, data) {
		return TodoManager.update(this.userId, id, data);
	},

	insertAfter: function(id, targetId) {
		return TodoManager.insertAfter(this.userId, id, targetId);
	}
});


Meteor.startup(function() {
	// REST
	RESTstop.configure({use_auth: true});

	// List of todos
	// GET /todos
	RESTstop.add('todos', {require_login: true, method: 'GET'}, function() {
		return [Todos.find({userId: this.user._id}, {sort: {order: 1}}).fetch()];
	});

	// Single todo
	// GET /todos/:id
	RESTstop.add('todos/:id', {require_login: true, method: 'GET'}, function() {
		return TodoManager.get(this.user._id, this.params.id) || 404;
	});

	// Add todo
	// POST /todos
	RESTstop.add('todos', {require_login: true, method: 'POST'}, function() {
		var id = TodoManager.add(this.user._id, this.params);
		if (id) return [201, TodoManager.get(this.user._id, id)];
		else return 400;
	});

	// Remove todo
	// DELETE /todos/:id
	RESTstop.add('todos/:id', {require_login: true, method: 'DELETE'}, function() {
		TodoManager.remove(this.user._id, this.params.id);
		return 200;
	});

	// Update todo
	// PUT /todos/:id
	RESTstop.add('todos/:id', {require_login: true, method: 'PUT'}, function() {
		var result = TodoManager.update(this.user._id, this.params.id, {title: this.params.title});
		if (result) return TodoManager.get(this.user._id, this.params.id);
		else return 400;
	});
});
